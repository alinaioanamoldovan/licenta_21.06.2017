package com.example.demo.validate;

import com.example.demo.model.LocalUser;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


/**
 * Created by Toshiba on 6/3/2017.
 */
@Component
public class UserValidate implements Validator {


    @Override
    public boolean supports(Class<?> aClass) {
        return LocalUser.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        LocalUser user = (LocalUser) o;

        String password = user.getPassword();
        String confPassword = user.getRepeatPassword();

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "", "L'usuari no pot estar en blanc");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "", "La contrasenya no pot estar en blanc");

        // Comprova les contrasenyes
        if(!password.equals(confPassword)){
            errors.rejectValue("password","", "Les contrasenyes no són iguals");
        }
    }
}
