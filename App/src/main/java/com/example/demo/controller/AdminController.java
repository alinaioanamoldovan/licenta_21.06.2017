package com.example.demo.controller;

import com.example.demo.model.LocalUser;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import com.example.demo.validate.UserValidate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.imageio.ImageIO;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * Created by Toshiba on 6/4/2017.
 */

@Controller
@RequestMapping("/admin")
public class AdminController {

    @RequestMapping("/home")
    public String home()
    {
        return "adminHome";
    }

    private static final Log log = LogFactory.getLog(AdminController.class);

    @Autowired
    UserValidate userValidate;

//    @Autowired
    //  MailService mailClient;


    @RequestMapping("/where")
    public String where()
    {
        return "where";
    }

    @RequestMapping("/about")
    public String about()
    {
        return "about";
    }
    @Autowired
    UserService userService;
    @InitBinder("user")
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(userValidate);
    }

    @RequestMapping("/profile")
    public String viewProfile(Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

        User user = userService.findByUsername(username);

        if (user == null) {
            log.error("User " + username + " does not exist");
            model.addAttribute("error", "User does not exist");
            return "error";
        }

        log.info("View profile" + username);
        model.addAttribute("user", username);
        model.addAttribute("user2",user);
        model.addAttribute("localUser", user.getUser());
        return "account";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.POST)
    public String updateProfile(@ModelAttribute("user") @Valid LocalUser user, BindingResult result, @RequestParam(value = "icon", required = false) MultipartFile icon)
    {
        String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

        if (!icon.isEmpty()) {

            try
            {
                if (!validaImatge(username,icon))
                {
                    result.rejectValue("icon", "", "Incorrect image");
                    log.info("Incorrect image (" + user.getUsername() + ")");
                    return "account";
                }
            }
            catch (IOException e) {
                e.printStackTrace();
                result.rejectValue("icon", "", "Problem saving image");
                log.info("IO Error (" + user.getUsername() + ")");
                return "account";
            }
        }

        if (result.hasErrors()) {
            log.error("Error updating profile");
            return "account";
        }

        log.info("Updating image: " + username);
        userService.updateUser(username, user);
        return "redirect:/admin/profile";
    }

    private boolean validaImatge(String usuari, MultipartFile image) throws IOException {
        try (InputStream input = image.getInputStream()) {
            try {
                // No n'estic segur de que es pugui llegir dos cops ..
                BufferedImage buf = ImageIO.read(input);
                log.info("Saving image");
                ImageIO.write(buf, "png", new File("icons/", usuari + ".png"));
                return true;
            } catch (Exception e) {
                log.info("Error " + e.getMessage());
            }
        }
        return false;
    }

    @RequestMapping("/profile/image")
    @ResponseBody
    public FileSystemResource getImage() {
        String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

        FileSystemResource resource;
        resource = new FileSystemResource("icons/"  + username + ".png");
        if (!resource.exists()) {
            log.info(resource.getFilename() + " does not exist");
            return new FileSystemResource("icons/x.png");
        }
        return resource;
    }



}
