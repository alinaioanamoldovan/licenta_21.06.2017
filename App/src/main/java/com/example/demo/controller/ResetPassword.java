package com.example.demo.controller;

import com.example.demo.model.LocalUser;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.UUID;



/**
 * Created by Toshiba on 6/4/2017.
 */
@Controller
public class ResetPassword {

    private static final Log log = LogFactory.getLog(ResetPassword.class);
    @Autowired
    JavaMailSender mailSender;
    @Autowired
    UserService userService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @RequestMapping("/forget-password")
    public String forget(Model model)
    {
        //LocalUser user = new LocalUser();
        //model.addAttribute("user",user);
        return "/forget-password";
    }

    @RequestMapping(value = "/forget-password",method = RequestMethod.POST)
    public ModelAndView forget(@RequestParam(value = "username") String username)
    {

        ModelAndView model = new ModelAndView();
        User user = userService.findByUsername(username);
        if (user == null) {
            log.info("username null");
            model.addObject("errorMessage", "Email not found, please check your email again.");
            model.setViewName("forget-password");
        }

        String uuid = UUID.randomUUID().toString();
        user.setValidateCode(uuid);
        userService.updateUser(user.getUsername(),user.getUser());

        //given
        String recipient = user.getEmail();
        String subject = "Reset password ";
        String message = "Please click here "+"http://localhost:8080"+"/reset-password/"+uuid+" to verify your account. Thank you!";
        //when
        //when
        SimpleMailMessage message2 = new SimpleMailMessage();

        message2.setSubject(subject);
        message2.setText(message);
        message2.setTo(recipient);
        message2.setFrom("alina.moldovan12@gmail.com");
        try {
            mailSender.send(message2);
            System.out.println("{\"message\": \"OK\"}");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print("{\"message\": \"Error\"}");
        }

        System.out.println("Message received: " + message);


        model.addObject("successMessage", "Please check your email to reset your password");
        model.setViewName("forget-password");



        return model;
    }

    @RequestMapping(value = "/resetPassword/{code}", method = RequestMethod.GET)
    public ModelAndView verifyResetCode(@PathVariable("code") String code) {
        ModelAndView modelAndView = new ModelAndView();
        User user = userService.findUserByValidateCode(code);
        if (user == null)  {
            modelAndView.addObject("successMessage", "Sorry, code is not valid.");
            modelAndView.setViewName("login");
        } else {
            modelAndView.addObject("code", code);
            modelAndView.setViewName("reset-password");
        }
        return modelAndView;
    }

    @RequestMapping(value = "/reset-password", method = RequestMethod.POST)
    public ModelAndView verifyStudent(@RequestParam(value = "code") String code, @RequestParam(value = "password") String password, @RequestParam(value = "password2") String password2) {
        ModelAndView modelAndView = new ModelAndView();
        User user = userService.findUserByValidateCode(code);
        if (user == null)  {
            modelAndView.addObject("errorMessage", "Sorry, code is not valid.");
            modelAndView.setViewName("reset-password");
        }

        else if(password.length()<6){
            modelAndView.addObject("errorMessage", "Password should be more than 6 characters.");
            modelAndView.addObject("code", code);
            modelAndView.setViewName("reset-password");
        }

        else if(!password.equals(password2)){
            modelAndView.addObject("errorMessage", "Password does not match. Please check again");
            modelAndView.addObject("code", code);
            modelAndView.setViewName("reset-password");
        }

        else {
            String uuid = null;
            user.setValidateCode(uuid);
            user.setPassword(bCryptPasswordEncoder.encode(password + user.getSalt()));
            userService.updateUser(user.getUsername(),user.getUser());


            modelAndView.addObject("successMessage", "Your password was reset successfully. Now please login..");
            //modelAndView.addObject("user", user);
            modelAndView.setViewName("login");

        }
        return modelAndView;
    }
}
