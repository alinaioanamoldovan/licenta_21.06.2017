package com.example.demo.controller;

import com.example.demo.model.Conference;
import com.example.demo.service.ConferenceService;
import com.example.demo.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Toshiba on 6/3/2017.
 */
@Controller
public class HomeController {

    private static final Log log = LogFactory.getLog(HomeController.class);


    @RequestMapping("/where")
    public String where()
    {
        return "where";
    }

    @RequestMapping("/about")
    public String about()
    {
        return "about";
    }
    @Autowired
    ConferenceService conferenceService;

    @RequestMapping("/")
    public String home(Model model)
    {

        List<Conference> conferences = conferenceService.getAll();

        model.addAttribute("conferences",conferences);
        log.info("Home page");
        return "home";
    }
    @RequestMapping(value="/login", method= RequestMethod.GET)
    public String login(@RequestParam(value="error",required=false) String error,
                        HttpServletRequest request) {

        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() != "anonymousUser") {
            return "redirect:/";
        }
        return "login";
    }
    @RequestMapping(value = "/default", method = RequestMethod.GET)
    public ModelAndView defaultAfterLogin() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String role = auth.getAuthorities().toString();

        String targetUrl = "";
        if (role.contains("ADMIN")) {
            targetUrl = "redirect:/admin/home";
        } else if (role.contains("AUTHOR")) {
            targetUrl = "redirect:/author/home";

        } else if (role.contains("USER")) {
            targetUrl = "redirect:/";


        } else if (role.contains("REVIEWER"))
        {
            targetUrl = "redirect:/reviewer/home";
        }

        else {
            targetUrl = "redirect:/access-denied";
        }
        return new ModelAndView(targetUrl);
    }
}
