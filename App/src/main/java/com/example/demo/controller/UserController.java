package com.example.demo.controller;

import com.example.demo.model.LocalUser;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import com.example.demo.validate.UserValidate;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Message;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.imageio.ImageIO;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Toshiba on 6/3/2017.
 */
@Controller
public class UserController {
    public static final String ACCOUNT_SID = "AC13b3a9d7951a48bfe523cf9afae4a36d";
    public static final String AUTH_TOKEN = "7d81e0691b304790d21da42942d69332";
    public static final String TWILIO_NUMBER = "+1830-243-5094";


    private static final Log log = LogFactory.getLog(UserController.class);
    @Autowired
    JavaMailSender mailSender;
    @Autowired
    UserValidate userValidate;

//    @Autowired
  //  MailService mailClient;

    @InitBinder("user")
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(userValidate);
    }

    @RequestMapping("/profile")
    public String viewProfile(Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

        User user = userService.findByUsername(username);

        if (user == null) {
            log.error("User " + username + " does not exist");
            model.addAttribute("error", "User does not exist");
            return "error";
        }

        log.info("View profile" + username);
        model.addAttribute("user", username);
        model.addAttribute("user2",user);
        model.addAttribute("localUser", user.getUser());
        return "account";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.POST)
    public String updateProfile(@ModelAttribute("user") @Valid LocalUser user, BindingResult result, @RequestParam(value = "icon", required = false) MultipartFile icon)
    {
        String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

        if (!icon.isEmpty()) {

            try
            {
                if (!validaImatge(username,icon))
                {
                    result.rejectValue("icon", "", "Incorrect image");
                    log.info("Incorrect image (" + user.getUsername() + ")");
                    return "account";
                }
            }
            catch (IOException e) {
                e.printStackTrace();
                result.rejectValue("icon", "", "Problem saving image");
                log.info("IO Error (" + user.getUsername() + ")");
                return "account";
            }
        }

        if (result.hasErrors()) {
            log.error("Error updating profile");
            return "account";
        }

        log.info("Updating image: " + username);
        userService.updateUser(username, user);
        return "redirect:/profile";
    }

    private boolean validaImatge(String usuari, MultipartFile image) throws IOException {
        try (InputStream input = image.getInputStream()) {
            try {
                // No n'estic segur de que es pugui llegir dos cops ..
                BufferedImage buf = ImageIO.read(input);
                log.info("Saving image");
                ImageIO.write(buf, "png", new File("icons/", usuari + ".png"));
                return true;
            } catch (Exception e) {
                log.info("Error " + e.getMessage());
            }
        }
        return false;
    }

    @RequestMapping("/profile/image")
    @ResponseBody
    public FileSystemResource getImage() {
        String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

        FileSystemResource resource;
        resource = new FileSystemResource("icons/"  + username + ".png");
        if (!resource.exists()) {
            log.info(resource.getFilename() + " does not exist");
            return new FileSystemResource("icons/x.png");
        }
        return resource;
    }

    @RequestMapping("/register")
    public String register(Model model)
    {
        model.addAttribute("user", new LocalUser());
        return "register";
    }

    @Autowired
    UserService userService;
    @RequestMapping(value = "/register",method = RequestMethod.POST)
    public String register(@Valid @ModelAttribute("user") LocalUser user, BindingResult result, Model model, final RedirectAttributes redirectAttributes)
    {
        if (result.hasErrors())
        {
            model.addAttribute("user",user);
            log.warn("Error in registering "+result.getFieldErrors());
            return "register";
        }

        if (userService.userExists(user.getUsername()))
        {
           result.rejectValue("username","","Username already exists");
           log.info("User exists "+user.getUsername());
           return "register";
        }

        userService.addUser(user);
        String uuid = UUID.randomUUID().toString();
        user.setValidationCode(uuid);
        user.setEmailValid(0);

        //String localhost =
        //given

        String recipient = user.getEmail();
        String subject = "Verify your email";
        String message = "Thank you for registering. Please click here "+"http://localhost:8080"+"/verify/"+uuid+" to verify your account. Thank you! - LinkedU";
        //when
        String info = "Thanks for registering here is your info "+user.toString();
        String body = message+"\'n"+info;
        sendSMS(user.getTelefon(),info);
        SimpleMailMessage message2 = new SimpleMailMessage();

        message2.setSubject(subject);
        message2.setText(body);
        message2.setTo(recipient);
        message2.setFrom("alina.moldovan12@gmail.com");
        try {
            mailSender.send(message2);
            System.out.println("{\"message\": \"OK\"}");
        } catch (Exception e) {
            e.printStackTrace();
            return "{\"message\": \"Error\"}";
        }

        System.out.println("Message received: " + message);
        redirectAttributes.addFlashAttribute("message","User created!");

        return "redirect:/profile";
    }

    @RequestMapping(value = "/verify/{code}", method = RequestMethod.GET)
    public ModelAndView verify(@PathVariable("code") String code) {
        ModelAndView modelAndView = new ModelAndView();
        User user = userService.findUserByValidateCode(code);
        if (user == null)  {
            modelAndView.addObject("successMessage", "Sorry, code is not valid.");
            modelAndView.setViewName("login");
        } else {
            String uuid = null;
            user.setValidateCode(uuid);
            user.setEmailValid(1);
            userService.updateUser(user.getUsername(),user.getUser());


            modelAndView.addObject("successMessage", "Your account verified successfully. Now please login..");
            //modelAndView.addObject("user", user);
            modelAndView.setViewName("login");

        }
        return modelAndView;
    }
    @RequestMapping("/register-admin")
    public String registerA(Model model)
    {
        model.addAttribute("user", new LocalUser());
        return "register-admin";
    }


    @RequestMapping(value = "/register-admin",method = RequestMethod.POST)
    public String registerAdmin(@Valid @ModelAttribute("user") LocalUser user, BindingResult result, Model model, final RedirectAttributes redirectAttributes)
    {
        if (result.hasErrors())
        {
            model.addAttribute("user",user);
            log.warn("Error in registering "+result.getFieldErrors());
            return "register-admin";
        }

        if (userService.userExists(user.getUsername()))
        {
            result.rejectValue("username","","Username already exists");
            log.info("User exists "+user.getUsername());
            return "register-admin";
        }

        userService.addAdmin(user);
        String uuid = UUID.randomUUID().toString();
        user.setValidationCode(uuid);
        user.setEmailValid(0);

        //String localhost =
        //given
        String recipient = user.getEmail();
        String subject = "Verify your email";
        String message = "Thank you for registering. Please click here "+"http://localhost:8080"+"/verify/"+uuid+" to verify your account. Thank you! - LinkedU";
        //when

        String info = "Thanks for registering here is your info "+user.toString();
        sendSMS(user.getTelefon(),info);
        String body = message+"\'n"+info;
        SimpleMailMessage message2 = new SimpleMailMessage();

        message2.setSubject(subject);
        message2.setText(body);
        message2.setTo(recipient);
        message2.setFrom("alina.moldovan12@gmail.com");
        try {
            mailSender.send(message2);
            System.out.println("{\"message\": \"OK\"}");
        } catch (Exception e) {
            e.printStackTrace();
            return "{\"message\": \"Error\"}";
        }

        System.out.println("Message received: " + message);
        redirectAttributes.addFlashAttribute("message","User created!");

        return "redirect:/admin/profile";
    }

    @RequestMapping("/register-author")
    public String registerAuth(Model model)
    {
        model.addAttribute("user", new LocalUser());
        return "register-author";
    }

    @RequestMapping(value = "/register-author",method = RequestMethod.POST)
    public String registerAu(@Valid @ModelAttribute("user") LocalUser user, BindingResult result, Model model, final RedirectAttributes redirectAttributes)
    {
        if (result.hasErrors())
        {
            model.addAttribute("user",user);
            log.warn("Error in registering "+result.getFieldErrors());
            return "register-author";
        }

        if (userService.userExists(user.getUsername()))
        {
            result.rejectValue("username","","Username already exists");
            log.info("User exists "+user.getUsername());
            return "register-author";
        }

        userService.addAuthor(user);
        String uuid = UUID.randomUUID().toString();
        user.setValidationCode(uuid);
        user.setEmailValid(0);

        //String localhost =
        //given
        String recipient = user.getEmail();
        String subject = "Verify your email";
        String message = "Thank you for registering. Please click here "+"http://localhost:8080"+"/verify/"+uuid+" to verify your account. Thank you! - LinkedU";
        //when

        String info = "Thanks for registering here is your info "+user.toString();
        sendSMS(user.getTelefon(),info);
        String body = message+"\'n"+info;
        SimpleMailMessage message2 = new SimpleMailMessage();

        message2.setSubject(subject);
        message2.setText(body);
        message2.setTo(recipient);
        message2.setFrom("alina.moldovan12@gmail.com");
        try {
            mailSender.send(message2);
            System.out.println("{\"message\": \"OK\"}");
        } catch (Exception e) {
            e.printStackTrace();
            return "{\"message\": \"Error\"}";
        }

        System.out.println("Message received: " + message);
        redirectAttributes.addFlashAttribute("message","User created!");

        return "redirect:/author/profile";
    }

    @RequestMapping("/register-reviewer")
    public String registerReviewer(Model model)
    {
        model.addAttribute("user", new LocalUser());
        return "register-reviewer";
    }

    @RequestMapping(value = "/register-reviewer",method = RequestMethod.POST)
    public String registerRev(@Valid @ModelAttribute("user") LocalUser user, BindingResult result, Model model, final RedirectAttributes redirectAttributes)
    {
        if (result.hasErrors())
        {
            model.addAttribute("user",user);
            log.warn("Error in registering "+result.getFieldErrors());
            return "register-reviewer";
        }

        if (userService.userExists(user.getUsername()))
        {
            result.rejectValue("username","","Username already exists");
            log.info("User exists "+user.getUsername());
            return "register-reviewer";
        }

        userService.addReviewer(user);
        String uuid = UUID.randomUUID().toString();
        user.setValidationCode(uuid);
        user.setEmailValid(0);

        //String localhost =
        //given
        String recipient = user.getEmail();
        String subject = "Verify your email";
        String message = "Thank you for registering. Please click here "+"http://localhost:8080"+"/verify/"+uuid+" to verify your account. Thank you! - LinkedU";
        //when

        String info = "Thanks for registering here is your info "+user.toString();
        sendSMS(user.getTelefon(),info);
        String body = message+"\'n"+info;
        SimpleMailMessage message2 = new SimpleMailMessage();

        message2.setSubject(subject);
        message2.setText(body);
        message2.setTo(recipient);
        message2.setFrom("alina.moldovan12@gmail.com");
        try {
            mailSender.send(message2);
            System.out.println("{\"message\": \"OK\"}");
        } catch (Exception e) {
            e.printStackTrace();
            return "{\"message\": \"Error\"}";
        }

        System.out.println("Message received: " + message);
        redirectAttributes.addFlashAttribute("message","User created!");

        return "redirect:/reviewer/profile";
    }

    public void sendSMS(String phoneNumber,String body) {
        try {
            TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);

            // Build a filter for the MessageList
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("Body", body));
            params.add(new BasicNameValuePair("To", phoneNumber)); //Add real number here
            params.add(new BasicNameValuePair("From", TWILIO_NUMBER));

            MessageFactory messageFactory = client.getAccount().getMessageFactory();
            Message message = messageFactory.create(params);
            System.out.println(message.getSid());
        }
        catch (TwilioRestException e) {
            System.out.println(e.getErrorMessage());
            e.printStackTrace();
        }
    }

    @RequestMapping("/deleteAccount/{id}")
    public String deleteAccount(@PathVariable("id")String id)
    {
        User user = userService.findOne(id);
        userService.deleteUser(user);
        return "redirect:/login";
    }
}
