package com.example.demo.controller;

import com.example.demo.model.Conference;
import com.example.demo.model.User;
import com.example.demo.repositories.ConferenceRepository;
import com.example.demo.service.ConferenceService;
import com.example.demo.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Toshiba on 6/17/2017.
 */
@Controller
@RequestMapping("/admin")
public class ConferenceController {
    private static final Log log = LogFactory.getLog(ConferenceController.class);

    @Autowired
    ConferenceService conferenceService;

    @Autowired
    UserService userService;
    @RequestMapping("/viewAll")
    public String viewAll(Model model)
    {
        List<Conference> conferences = conferenceService.getAll();

        model.addAttribute("conferences",conferences);

        return "viewAll";
    }

    @RequestMapping("/view/{id}")
    public String viewConference(Model model, @PathVariable("id") String id)
    {
        Conference conference = conferenceService.getById(id);
        model.addAttribute("conference",conference);
        return "view";
    }

    @RequestMapping(value = "/createConference",method = RequestMethod.GET)
    public String createConference(Model model)
    {
        model.addAttribute("conference",new Conference());
        return "createConference";
    }

    @RequestMapping(value = "/createConference",method = RequestMethod.POST)
    public String createConferencePost(@Valid @ModelAttribute("conference") Conference conference, BindingResult result,RedirectAttributes redirAttr,Model model)
    {
        if (result.hasErrors())
        {
            log.info(result.toString());
            System.out.print(result.toString());
            return "createConference";
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUsername = authentication.getName();
        User user = userService.findByUsername(currentUsername);
        conference.setUser(user);
        conferenceService.addConference(conference);

        redirAttr.addFlashAttribute("flashMessage", "Conference created");

        return "redirect:/admin/home";
    }

    @RequestMapping(value="/update/{id}",method = RequestMethod.GET)
    public String update(Model model,@PathVariable("id") String id)
    {
        Conference conference = conferenceService.getById(id);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUsername = authentication.getName();
        User user = userService.findByUsername(currentUsername);
        conference.setUser(user);

        if (conference == null || conference.getUser().getId() != user.getId()) {
            throw new AccessDeniedException("This conference cannot be accessed");
        }

        //conferenceService.update(conference);
        model.addAttribute("conference",conference);
        return "updateConference";
    }

    @RequestMapping(value="/update",method=RequestMethod.POST)
    public String updatePost(@Valid @ModelAttribute("conference")Conference conference,
                             BindingResult result, Model model,
                             RedirectAttributes redirAttr)
    {

    //    Conference old = conferenceService.getById(conference.getId());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUsername = authentication.getName();
        User user = userService.findByUsername(currentUsername);
      /*  if (old == null || old.getUser().getId() != user.getId()) {
            throw new AccessDeniedException("This conference cannot be accessed");
        }*/

        if (result.hasErrors())
        {
            log.error(result.toString());
            System.out.print(result.toString());
            return "updateConference";
        }

        /*old.setBeginDate(conference.getBeginDate());
        old.setBeginSubmissions(conference.getBeginSubmissions());
        old.setEndDate(conference.getEndDate());
        old.setEndReview(conference.getEndReview());
        old.setEndSubmissions(conference.getEndSubmissions());
        old.setName(conference.getName());
       */
        conference.setUser(user);
        conferenceService.update(conference);


        return "redirect:/admin/viewAll";
    }

    @RequestMapping("/deleteConference/{id}")
    public String deleteConference(@PathVariable("id")String id)
    {
        Conference conference = conferenceService.getById(id);
        conferenceService.delete(conference);
        return "redirect:/admin/viewAll";
    }
}
