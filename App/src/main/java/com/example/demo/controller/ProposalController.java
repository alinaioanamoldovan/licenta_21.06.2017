package com.example.demo.controller;

import com.example.demo.model.Conference;
import com.example.demo.model.Proposal;
import com.example.demo.model.Topic;
import com.example.demo.model.User;
import com.example.demo.repositories.TopicRepository;
import com.example.demo.service.ConferenceService;
import com.example.demo.service.ProposalService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.security.acls.model.NotFoundException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Toshiba on 6/18/2017.
 */
@Controller
public class ProposalController {


    @Autowired
    ConferenceService conferenceService;
    @Autowired
    ProposalService proposalService;

    @Autowired
    UserService userService;

    @Autowired
    TopicRepository topicRepository;
    @RequestMapping("/viewAll")
    public String viewAll(Model model)
    {
        List<Conference> conferences = conferenceService.getAll();

        model.addAttribute("conferences",conferences);

        return "viewAll";
    }

    @RequestMapping("/viewDetails/{id}")
    public String viewConference(Model model, @PathVariable("id") String id)
    {
        Conference conference = conferenceService.getById(id);
        model.addAttribute("conference",conference);
        return "viewDetails";
    }
    @RequestMapping(value = "/createSubmission/{id}",method = RequestMethod.GET)
    public String createSubmission(@PathVariable("id")String id, Model model, HttpSession session)
    {
        Conference conference = conferenceService.getById(id);

        if (conference== null)
        {
            throw new NotFoundException("Conference Not Found");
        }

        if (Calendar.getInstance().compareTo(conference.getEndSubmissions())==1)
        {
            throw new NotFoundException("Submissions period ended");
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUsername = authentication.getName();
        User user = userService.findByUsername(currentUsername);
        Topic topic = new Topic();
        topic.setName("Computer science");
        topicRepository.insert(topic);
        List<Topic> topics = topicRepository.findAll();
        //topics.add(topic);
        model.addAttribute("id",id);
        model.addAttribute("conference",conference);
        model.addAttribute("proposal",new Proposal());
        model.addAttribute("topics",topics);


        return "createSubmission";
    }

    Path path;
    @RequestMapping(value="/createSubmission/{id}",method = RequestMethod.POST)
    public String createSubmissionPost(@PathVariable("id")String id, @Valid @ModelAttribute("proposal") Proposal proposal, BindingResult result, Model model, RedirectAttributes redirectAttributes)
    {
        if (id==null)
        {
            System.out.print("Aici");
        }
        Conference conference = conferenceService.getById(id);
      //  model.addAttribute("conference",conference);
        if (conference == null)
        {
            System.out.print(conference.toString());
            throw new NotFoundException("The conference does not exist!");
        }
        if (Calendar.getInstance().compareTo(conference.getEndSubmissions()) != 0) {
            throw new NotFoundException("Submission period is over");
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUsername = authentication.getName();
        User user = userService.findByUsername(currentUsername);

        if (result.hasErrors())
        {
            //model.addAttribute("topics",topicRepository.findAll());
            model.addAttribute("id", id);
            System.out.print(result.toString());
            return "createSubmission";
        }

        proposal.setUser(user);
        proposal.setConference(conference);

        proposalService.addProposal(proposal);

        MultipartFile file = proposal.getFile();
        path = Paths.get( "papers/" + proposal.getUser().getUsername());

        if (file!=null && !file.isEmpty())
        {
            try {
                file.transferTo(new File("papers"+proposal.getUser().getUsername()));
            } catch (IOException e) {
                e.printStackTrace();
                throw  new RuntimeException("File saving failed",e);
            }
        }

        redirectAttributes.addFlashAttribute("flashMessage", "The proposal was successfully added");

        return "redirect:/";




    }
}
