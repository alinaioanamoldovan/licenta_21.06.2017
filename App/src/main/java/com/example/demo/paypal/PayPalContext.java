package com.example.demo.paypal;

import com.paypal.base.rest.APIContext;

import static com.example.demo.paypal.PayPalConfig.clientId;
import static com.example.demo.paypal.PayPalConfig.clientSecret;

/**
 * Created by Toshiba on 6/6/2017.
 */
public class PayPalContext {

    public static APIContext context = new APIContext(clientId, clientSecret, "sandbox");
}
