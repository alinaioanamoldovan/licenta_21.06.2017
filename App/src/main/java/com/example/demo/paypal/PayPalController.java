package com.example.demo.paypal;

/**
 * Created by Toshiba on 6/6/2017.
 */
/*
@RestController
@RequestMapping("/api")
public class PayPalController {

    private static final Log log = LogFactory.getLog(PayPalController.class);


    /* @Autowired
     ParticipantInInsuranceRepository participantInInsuranceRepository;

     @Autowired
     HomeInsuranceRepository homeInsuranceRepository;

     @Autowired
     CarInsuranceRepository carInsuranceRepository;


     @Autowired
     TravelInsuranceService travelInsuranceService;

     @Autowired
     TravelInsuranceRepository travelInsuranceRepository;
 */
/*    @Autowired
    PayPalService payPalService;

    @Autowired
    PriceCalculatorService priceCalculatorService;

    @Autowired
    MailService mailService;

    /**
     * When user clicks on BUY ITEM button
     *
     * @see PayPalService
 /*    */
   /* @RequestMapping(value = "/paypal/create",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createPayment(@RequestBody String stringTicket) {

        log.debug("Request for creating payment for travel insurance {}" + stringTicket);

        Ticket travelInsurance = null;
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        String cleanedStringTravelInsurance = Jsoup.clean(stringTicket, Whitelist.basic());

        if (!stringTicket.equals(cleanedStringTravelInsurance)) {
            log.debug("Suspicious content of json object: {}" + stringTicket);

            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        // Parse json object to appropriate java object
        try {
            travelInsurance = gson.fromJson(cleanedStringTravelInsurance, Ticket.class);
        } catch (Exception e) {
            log.debug("Error during parsing json for travel insurance: {}", e);

            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        log.debug("Request for creating payment for travel insurance with {} ID" + travelInsurance.getId());

        if (travelInsurance.getId() != null) {
            log.debug("ID for travel insurance is not null.");

            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        /*
        if (!travelInsuranceService.validation(travelInsurance)) {
            LOG.debug("Request for creating failed. TravelInsurance is not valid");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        */
        /*try {
            /* [1] calculate final price and save entity do DB*/

            // Double calculatedTotalPrice = priceCalculatorService.calculate(travelInsurance);

            //travelInsurance.setTotalPrice();

            //TravelInsurance savedTravelInsurance = travelInsuranceService.save(travelInsurance);


            //----------------------------------------------------------------------------------
            /*for(ParticipantInInsurance pi: travelInsurance.getParticipantInInsurances()){
                pi.setTravelInsurance(travelInsurance);
            }
            participantInInsuranceRepository.save(travelInsurance.getParticipantInInsurances());
            if(travelInsurance.getCarInsurances() != null) {
                for (CarInsurance ci : travelInsurance.getCarInsurances()) {
                    ci.setTravelInsurance(travelInsurance);
                }
                carInsuranceRepository.save(travelInsurance.getCarInsurances());
            }*/
            /// if(travelInsurance.getHomeInsurances() != null) {
            //  for (HomeInsurance hi : travelInsurance.getHomeInsurances()) {
            //    hi.setTravelInsurance(travelInsurance);
            //}
            //homeInsuranceRepository.save(travelInsurance.getHomeInsurances());
  /*      } catch (Exception e) {
        }



        //-----------------------------------------------------------------------------------


        //log.debug("Saved with {} ID", savedTravelInsurance.getId());

            /* [2] get paypal link whre user will be redirected - create payment*/
        //Payment payment = payPalService.createPayment(travelInsurance.getId(), 100, 0, 100, "Travel Insurance Package by Travel Safe, Inc.");

    /*    if (payment != null) {
            Links links = payPalService.getLink(payment);
            if (links != null) {

                    /* [3] travelinsurance update - set payment id*/
                //savedTravelInsurance.setPaypalPaymentId(payment.getId());
                //travelInsuranceRepository.save(savedTravelInsurance);

  /*                  /* [4] pack data to response*/
          //      HashMap<String, Object> response = new HashMap<>();
            //    response.put("link", links);
             //   return new ResponseEntity<>(response, HttpStatus.OK);*/
/*
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
*/


    //   catch (Exception e) {
    //     log.debug("Exception during calculating price: {}", e);

    //   return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

    //return null;


    /**
     * When users is redirected to our website to confirm payment (execute it) or cancel it (delte order from DB)
     * Same url in both cases - angular opusteno :D
     *
     * @see PayPalService
     */
    /*@RequestMapping(value = "/paypal/execute/{orderId}/{paymentId}/{payerId}",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity executePayment(@PathVariable Long orderId, @PathVariable String paymentId, @PathVariable String payerId) {

        log.debug("Execute payment with {} order ID, {} payment ID and {} payer ID.");

        if (!paymentId.equals(Jsoup.clean(paymentId, Whitelist.basic())) || !payerId.equals(Jsoup.clean(payerId, Whitelist.basic()))) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

}

    //TravelInsurance order = travelInsuranceRepository.findOne(orderId);

    //order exists
    //order and payment are matching combo
    //*if(order!=null && payPalService.checkOrderAndPaymentCombo(orderId, paymentId)){

       /*     boolean status = payPalService.executePayment(payerId, paymentId);
            log.debug("Status of payment {}",status);
            if(status){
                //update order
                //order.setTimeOfPayment(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss z").format(ZonedDateTime.now(ZoneOffset.UTC)));
                order.setPayed(true);
               // travelInsuranceRepository.save(order);
                //mailService.sendMailWithAttachment(order);
                return new ResponseEntity<>(HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }*/


   /* @RequestMapping(value = "/paypal/cancel/{orderId}/{paymentId}/{payerId}",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity cancelPaymet(@PathVariable Long orderId, @PathVariable String paymentId, @PathVariable String payerId) {

        log.debug("Cancel payment with {} order ID, {} payment ID and {} payer ID.");

        //TravelInsurance order = travelInsuranceRepository.getOne(orderId);

        //order exists
        //order and payment are matching combo
        //   if(order!=null && payPalService.checkOrderAndPaymentCombo(orderId, paymentId)){
        //  travelInsuranceRepository.delete(orderId);
        return new ResponseEntity<>(HttpStatus.OK);
    }*/

    // return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    //return null;
//}


//}