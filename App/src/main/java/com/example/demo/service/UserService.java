package com.example.demo.service;

import com.example.demo.model.LocalUser;
import com.example.demo.model.User;
import com.example.demo.repositories.UserRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by Toshiba on 6/3/2017.
 */
@Service
public class UserService {
    private static final Log log = LogFactory.getLog(UserService.class);

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public User addUser(LocalUser user)
    {
        User userN =  new User();
        userN.setUsername(user.getUsername());
        userN.setPassword(passwordEncoder.encode(user.getPassword()+userN.getSalt()));
        userN.setRole("USER");
        userN.setFirstName(user.getFirstName());
        userN.setLastName(user.getLastName());
        userN.setAffiliation(user.getAffiliation());
        userN.setOcupation(user.getOcupation());
        userN.setEmail(user.getEmail());
        userN.setTelefon(user.getTelefon());
        userN.setAddress(user.getAddress());
        userN.setCountry(user.getCountry());
        //String uuid = UUID.randomUUID().toString();
        //userN.setValidateCode(uuid);
        return userRepository.save(userN);
    }
    public User addAdmin(LocalUser user)
    {
        User userN =  new User();
        userN.setUsername(user.getUsername());
        userN.setPassword(passwordEncoder.encode(user.getPassword()+userN.getSalt()));
        userN.setRole("ADMIN");
        userN.setFirstName(user.getFirstName());
        userN.setLastName(user.getLastName());
        userN.setAffiliation(user.getAffiliation());
        userN.setOcupation(user.getOcupation());
        userN.setEmail(user.getEmail());
        userN.setTelefon(user.getTelefon());
        userN.setAddress(user.getAddress());
        userN.setCountry(user.getCountry());
        //String uuid = UUID.randomUUID().toString();
        //userN.setValidateCode(uuid);
        return userRepository.save(userN);
    }
    public User addAuthor(LocalUser user)
    {
        User userN =  new User();
        userN.setUsername(user.getUsername());
        userN.setPassword(passwordEncoder.encode(user.getPassword()+userN.getSalt()));
        userN.setRole("AUTHOR");
        userN.setFirstName(user.getFirstName());
        userN.setLastName(user.getLastName());
        userN.setAffiliation(user.getAffiliation());
        userN.setOcupation(user.getOcupation());
        userN.setEmail(user.getEmail());
        userN.setTelefon(user.getTelefon());
        userN.setAddress(user.getAddress());
        userN.setCountry(user.getCountry());
        //String uuid = UUID.randomUUID().toString();
        //userN.setValidateCode(uuid);
        return userRepository.save(userN);
    }
    public User addReviewer(LocalUser user)
    {
        User userN =  new User();
        userN.setUsername(user.getUsername());
        userN.setPassword(passwordEncoder.encode(user.getPassword()+userN.getSalt()));
        userN.setRole("REVIEWER");
        userN.setFirstName(user.getFirstName());
        userN.setLastName(user.getLastName());
        userN.setAffiliation(user.getAffiliation());
        userN.setOcupation(user.getOcupation());
        userN.setEmail(user.getEmail());
        userN.setTelefon(user.getTelefon());
        userN.setAddress(user.getAddress());
        userN.setCountry(user.getCountry());
        //String uuid = UUID.randomUUID().toString();
        //userN.setValidateCode(uuid);
        return userRepository.save(userN);
    }
    public void deleteUser(User user)
    {
        userRepository.delete(user);
    }
    public void updateUser(String username,LocalUser userL)
    {
        User user = findByUsername(username);
        if (user!=null) {
            user.setUser(userL);
            userRepository.save(user);
        }
        else throw new RuntimeException();
    }

    public User findOne(String id)
    {
        return userRepository.findOne(id);
    }
    public boolean userExists(String username) {
        return userRepository.findByUsername(username) != null;
    }
    public List<User> findAll()
    {
        return userRepository.findAll();
    }
    public User findByUsername(String username)
    {
        return userRepository.findByUsername(username);
    }
    public User login(String username,String password)
    {
        User user = userRepository.findByUsername(username);
        if (user == null)
        {
            log.warn("User with" +username+" does not exist");
            return null;
        }

        String passE = password + user.getSalt();

        if (!passwordEncoder.matches(passE,user.getPassword()))
        {
            log.warn("Wrong password");
            return null;
        }

        return user;
    }

    public User findUserByValidateCode(String code)
    {
        return userRepository.findUserByValidateCode(code);
    }
}
