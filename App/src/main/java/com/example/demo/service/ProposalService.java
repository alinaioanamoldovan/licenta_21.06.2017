package com.example.demo.service;

import com.example.demo.model.Conference;
import com.example.demo.model.Proposal;
import com.example.demo.model.User;
import com.example.demo.repositories.ProposalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Toshiba on 6/18/2017.
 */
@Service
public class ProposalService {

    @Autowired
    ProposalRepository proposalRepository;

    public void addProposal(Proposal proposal)
    {
        proposalRepository.save(proposal);
    }

    public void updateProposal(Proposal proposal)
    {
        proposalRepository.save(proposal);
    }

    public  void deleteProposal(Proposal proposal)
    {
        proposalRepository.delete(proposal);
    }
    public Proposal getProposalById(String id)
    {
        return proposalRepository.findOne(id);
    }
    public List<Proposal> getAll()
    {
        return proposalRepository.findAll();
    }
    public List<Proposal> getByUser(User user)
    {
        return proposalRepository.getByUser(user);
    }
    public List<Proposal> getByConference(Conference conference)
    {
        return proposalRepository.getByConference(conference);
    }
}
