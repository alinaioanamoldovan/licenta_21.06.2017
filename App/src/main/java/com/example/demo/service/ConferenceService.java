package com.example.demo.service;

import com.example.demo.model.Conference;
import com.example.demo.model.User;
import com.example.demo.repositories.ConferenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Toshiba on 6/16/2017.
 */
@Service
public class ConferenceService {

    @Autowired
    private ConferenceRepository conferenceRepository;


    public Conference addConference(Conference conference)
    {
        conferenceRepository.insert(conference);
        return conference;
    }

    public void update(Conference conference)
    {
        conferenceRepository.save(conference);
    }

    public void delete(Conference conference)
    {
        conferenceRepository.delete(conference);
    }
    public Conference getById(String conferenceId)
    {
        return conferenceRepository.findOne(conferenceId);
    }
    public List<Conference> getAll()
    {
        return conferenceRepository.findAll();
    }

    public List<Conference> getByUser(User user)
    {
        return conferenceRepository.getByUser(user);
    }
}
