package com.example.demo.repositories;

import com.example.demo.model.Topic;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Toshiba on 6/18/2017.
 */
@Repository
public interface TopicRepository extends MongoRepository<Topic,String> {
}
