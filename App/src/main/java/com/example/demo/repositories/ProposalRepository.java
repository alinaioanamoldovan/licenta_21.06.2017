package com.example.demo.repositories;

import com.example.demo.model.Conference;
import com.example.demo.model.Proposal;
import com.example.demo.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Toshiba on 6/18/2017.
 */
@Repository
public interface ProposalRepository extends MongoRepository<Proposal,String> {

    List<Proposal> getByUser(User user);
    List<Proposal> getByConference(Conference conference);
}
