package com.example.demo.repositories;

import com.example.demo.model.Conference;
import com.example.demo.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Toshiba on 6/16/2017.
 */
@Repository
public interface ConferenceRepository extends MongoRepository<Conference,String> {

    List<Conference> getByUser(User user);
}
