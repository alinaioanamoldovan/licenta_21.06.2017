package com.example.demo.repositories;

import com.example.demo.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Toshiba on 6/3/2017.
 */
@Repository
public interface UserRepository extends MongoRepository<User,String> {
    User findUserByValidateCode(String code);
    User findByUsername(String username);
}
