package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

/**
 * Created by Toshiba on 6/3/2017.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserMongoAuthenticatorProvider mongoAuthenticationProvider;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Definim quin és l'accés que cal per les determinades url.
     *
     */
    private CsrfTokenRepository csrfTokenRepository()
    {
        HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
        repository.setSessionAttributeName("_csrf");
        return repository;
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf()
                .csrfTokenRepository(csrfTokenRepository());
        http
                .authorizeRequests()
                .antMatchers("/",
                        "/home",
                        "/register-admin",
                        "/register-author",
                        "/register-reviewer",
                        "/forget-password",
                        "/where",
                        "/recomana",
                        "/products",
                        "/product/**",
                        "/basket",
                        "/about",
                        "/empty",
                        "/viewDetails/**",
                        "/register",
                        "/webjars/**").permitAll()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .antMatchers("/author/**").hasRole("AUTHOR")
                .antMatchers("/reviewer/**").hasRole("REVIEWER")
                //.antMatchers("/**").hasAuthority("ROLE_USER")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/default")
                .permitAll()
                .and()
                .logout()
                .permitAll();
               // .and()
                //.csrf().disable();

    /*http.
            authorizeRequests()
            .antMatchers("/").permitAll()
            .antMatchers("/login").permitAll()
            .antMatchers("/registration-student", "/verifyStudent/**").permitAll()
            .antMatchers("/registration-recruiter", "/verifyRecruiter/**").permitAll()
            .antMatchers("/forget-password", "/resetPassword/**", "/reset-password").permitAll()
            .antMatchers("/contact-us", "/about-us", "/terms", "/policies").permitAll()
            .antMatchers("/upload", "/file/**", "/image/**").hasAnyAuthority("STUDENT","RECRUITER")
            .antMatchers("/student/**").hasAuthority("STUDENT")
            .antMatchers("/admin/**").hasAuthority("ADMIN")
            .antMatchers("/recruiter/**").hasAuthority("RECRUITER")
            .anyRequest().authenticated()
            .and()
            .csrf().disable()
            .formLogin()
            .loginPage("/login").failureUrl("/login?error=true")
            .defaultSuccessUrl("/default")
            .usernameParameter("email")
            .passwordParameter("password")
            .and()
            .logout()
            .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
            .logoutSuccessUrl("/").and().exceptionHandling()
            .accessDeniedPage("/access-denied");*/
    }

    /**
     * Definim quines són les adreces que no necessiten seguretat.
     *
     * No és necessàri accés segur per les adreces de recursos bàsics:
     * CSS, Javascript, Imatges, tipus de lletres.
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/js/**","/css/**","/images/**","/fonts/**");
    }


    /**
     * Configuració de l'autenticació dels usuaris.
     *
     * Ho he provat de dues formes:
     *
     * 1. Afegint els usuaris amb l'autenticador
     * de memòria
     *
     *    auth.inMemoryAuthentication().withUser("usuari").password("contrasenya").roles("USER");
     *    auth.inMemoryAuthentication().withUser("admin").password("admin").roles("ADMIN");
     *
     * 2. Amb un validador personalitzat, MongoAuthenticationProvider, que identifica els usuaris
     * a la base de dades. Aquest és el que he deixat
     *
     * @param auth
     * @throws Exception
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

        auth.authenticationProvider(mongoAuthenticationProvider);
    }

}
