package com.example.demo.config;

/**
 * Created by Toshiba on 6/3/2017.
 */

import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;



@Component
public class UserMongoAuthenticatorProvider implements AuthenticationProvider {

    private static final Log log = LogFactory.getLog(UserMongoAuthenticatorProvider.class);

    @Autowired
    UserService userService;

    public UserMongoAuthenticatorProvider() {
        super();
    }

    @Override
    public Authentication authenticate(final Authentication authentication) {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        if (!StringUtils.hasText(password)) {
            log.warn("Username {}: no password provided" + username);
            return null;
        }
        // Localitza l'usuari
        User user = userService.login(username,password);
        if (user == null) {
            log.warn("User " + username + " does not exist");
            // Potser hauria de fer una cosa com throw new BadCredentialsException("user or password incorrect");
            return null;
        }

        List<GrantedAuthority> grantedAuths = user.getAuthorities();

        return new UsernamePasswordAuthenticationToken(
                user.getUsername(),
                user.getPassword(),
                grantedAuths);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}

