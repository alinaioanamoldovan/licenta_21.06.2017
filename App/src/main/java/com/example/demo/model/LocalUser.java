package com.example.demo.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Size;

/**
 * Created by Toshiba on 6/3/2017.
 */
@Data
@Getter
@Setter
@NoArgsConstructor
public class LocalUser {

    @Size(min=3, max=30, message="Minimum 3 characters")
    String username;

    @Size(min=6, message="Minimum 6 characters")
    String password;
    @Size(min=6)
    String repeatPassword;

    String firstName;
    String lastName;
    String affiliation;
    String ocupation;
    String email;
    String telefon;
    String address;
    String country;
    String validationCode;
    int emailValid;
    MultipartFile icon;


    @Override
    public String toString() {
        return "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", repeatPassword='" + repeatPassword + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", affiliation='" + affiliation + '\'' +
                ", ocupation='" + ocupation + '\'' +
                ", email='" + email + '\'' +
                ", telefon='" + telefon + '\'' +
                ", address='" + address + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
