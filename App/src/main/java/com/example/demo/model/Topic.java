package com.example.demo.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by Toshiba on 6/16/2017.
 */
@Document(collection = "topic")
@Data
@Getter
@Setter
@NoArgsConstructor
public class Topic {

    @Id
    String id;

    String name;


    public Topic(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Topic{" +
                "name='" + name + '\'' +
                '}';
    }
}
