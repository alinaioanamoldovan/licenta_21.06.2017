package com.example.demo.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Toshiba on 6/17/2017.
 */
@Document(collection = "proposal")
@Data
@Getter
@Setter
@NoArgsConstructor
public class Proposal {

    @Id
    String id;

    String name;
    String description;

    User user;
    Conference conference;
    String abstractText;
    String keywords;
    String topic;
    @Transient
    MultipartFile file;
    Calendar modified;
    Calendar created;
    String paperStatus;


    public Proposal(String name, String description, User author,Conference conference, String abstractText, String keywords, String topics, MultipartFile file, Calendar modified, Calendar created, String paperStatus) {
        this.name = name;
        this.description = description;
        this.user = author;
        this.conference = conference;
        this.abstractText = abstractText;
        this.keywords = keywords;
        this.topic = topics;
        this.file = file;
        this.modified = modified;
        this.created = created;
        this.paperStatus = paperStatus;
    }

//    @Override
    /*public String toString() {
        return "Proposal{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", author=" + user.getFirstName()+user.getLastName() +
                ", abstractText='" + abstractText + '\'' +
                ", keywords='" + keywords + '\'' +
                ", topics=" + topic +
                ", modified=" + modified +
                ", created=" + created +
                ", paperStatus='" + paperStatus + '\'' +
                '}';
    }*/
    /*public String getFormattedTopics() {
        String topics = "";

        if (this.topics != null) {
            for (Topic topic : this.topics) {
                topics += topic.getName() + ", ";
            }
        }

        if (!topics.isEmpty()) {
            topics = topics.substring(0, topics.length() - 2);
        }

        return topics;
    }*/
}
