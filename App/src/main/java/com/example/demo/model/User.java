package com.example.demo.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Toshiba on 6/3/2017.
 */
@Document(collection = "users")
@Data
@Getter
@Setter
public class User {

    @Id
    String id;
    String username;
    String password;
    String firstName;
    String lastName;
    String affiliation;
    String ocupation;
    String email;
    String telefon;
    String address;
    String country;

    String salt;
    String role;
    Date dataCreated;
    String validateCode;
    int emailValid;

    public User()
    {
        salt= UUID.randomUUID().toString();
        //validateCode=UUID.randomUUID().toString();
        dataCreated = new Date();
    }
    public List<GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorityList = new ArrayList<>();
        //for (String role : roles) {
            authorityList.add(new SimpleGrantedAuthority(role));
        //}
        return authorityList;
    }
    public void setUser(LocalUser user) {

        firstName = user.getFirstName();
        lastName = user.getLastName();
        affiliation = user.getAffiliation();
        ocupation = user.getOcupation();
        email = user.getEmail();
        telefon = user.getTelefon();
        address = user.getAddress();
        country = user.getCountry();
    }

    public LocalUser getUser() {

        LocalUser user = new LocalUser();

        user.setUsername(username);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setAffiliation(affiliation);
        user.setOcupation(ocupation);
        user.setEmail(email);
        user.setTelefon(telefon);
        user.setAddress(address);
        user.setCountry(country);

        return user;
    }

    @Override
    public String toString() {
        return
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", affiliation='" + affiliation + '\'' +
                ", ocupation='" + ocupation + '\'' +
                ", email='" + email + '\'' +
                ", telefon='" + telefon + '\'' +
                ", address='" + address + '\'' + '}';
    }
}
