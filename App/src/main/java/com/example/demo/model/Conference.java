package com.example.demo.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Calendar;

/**
 * Created by Toshiba on 6/16/2017.
 */
@Document(collection = "conference")
@Data
@Getter
@Setter
@NoArgsConstructor
public class Conference {

    @Id
    String id;

    String name;
    User user;
    String conferenceName;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    Calendar beginDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    Calendar endDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    Calendar beginSubmissions;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    Calendar endSubmissions;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    Calendar endReview;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    Calendar created;


    public Conference(String name,User programChair, String conferenceName, Calendar beginDate, Calendar endDate, Calendar beginSubmissions, Calendar endSubmissions, Calendar endReview, Calendar created) {
        this.name=name;
        this.user = programChair;
        this.conferenceName = conferenceName;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.beginSubmissions = beginSubmissions;
        this.endSubmissions = endSubmissions;
        this.endReview = endReview;
        this.created = created;
    }

    @Override
    public String toString() {
        return "Conference{" +
                "name="+name+'\''+
                "programChair=" + user +
                ", conferenceName='" + conferenceName + '\'' +
                ", beginDate=" + beginDate +
                ", endDate=" + endDate +
                ", beginSubmissions=" + beginSubmissions +
                ", endSubmissions=" + endSubmissions +
                ", endReview=" + endReview +
                ", created=" + created +
                '}';
    }
}
